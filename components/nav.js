
function Nav() {
    return <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="/">News Apps & Weather Forecast</a>
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navb">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <a className="nav-link" href="/business">Business</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/entertainment">Entertainment</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/health">Health</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/science">Science</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/sports">Sports</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/technology">Technology</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/weather">Weather Forecast</a>
                    </li>
                </ul>
                <form className="form-inline my-2 my-lg-0">
                    <input className="form-control mr-sm-2" type="text" placeholder="Search"></input>
                    <button className="btn btn-success my-2 my-sm-0" type="button">Search</button>
                </form>
            </div>
        </nav>
    </div>
}

export default Nav;
