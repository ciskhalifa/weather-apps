import fetch from 'isomorphic-unfetch'
import Head from 'next/head'
import Nav from '../components/nav'
import Link from 'next/link'

function Weather(props) {

    return <div>
        <Head>
            <title>Weather Apps</title>
            <meta charSet="utf-8"></meta>
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></link>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        </Head>
        <Nav />
        <br />
        <div className="container">
            <h1>Weather Forecast Hourly</h1>
            <br />
            <div className="row">
                {props.list.map(({ dt_txt, dt, weather, main, wind }) => (
                    <div className="col-md-6">
                        <div className="card mb-4">
                            <div className="card-header">
                                <h4>{props.city.name},{props.city.country} :  {dt_txt}</h4>
                            </div>
                            <div className="card-body">
                                <h5>Weather</h5>
                                <ul>
                                    <li>{weather[0].main}</li>
                                </ul>
                                <h5>Main</h5>
                                <ul>
                                    
                                    <li>Temp : {(parseInt(main.temp) - 273.15).toFixed(2)} °C</li>
                                    <li>Temp Min : {(parseInt(main.temp_min) - 273.15).toFixed(2)} °C</li>
                                    <li>Temp Max : {(parseInt(main.temp_max) - 273.15).toFixed(2)} °C</li>
                                    <li>Tekanan : {main.pressure} hpa</li>
                                    <li>Kelembaban : {main.humidity} %</li>
                                </ul>
                                <h5>Wind</h5>
                                <ul>
                                    <li>Speed : {wind.speed}</li>
                                    <li>Deg : {wind.deg}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    </div>
}

Weather.getInitialProps = async function (e) {
    const res = await fetch('https://api.openweathermap.org/data/2.5/forecast/daily?q=Bandung&APPID=d76b58f8618d640aeaeaf2861ea89343&cnt=6');
    const data = await res.json();

    return {
        list: data.list,
        city: data.city
    }
}

export default Weather;
