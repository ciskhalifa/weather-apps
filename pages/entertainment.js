import fetch from 'isomorphic-unfetch'
import Head from 'next/head'
import Nav from '../components/nav'
import Link from 'next/link'

function Entertainment(props) {
    return <div>
        <Head>
            <title>News Apps</title>
            <meta charSet="utf-8"></meta>
            <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></link>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        </Head>
        <Nav />
        <br />
        <div className="container">
            <h1>Entertainment News</h1>
            <br />
            <div className="row">
                {props.articles.map(({ title, source, urlToImage, url }) => (
                    <div className="col-md-4">
                        <div className="card mb-4">
                            <img className="bd-placeholder-img card-img-top" width="100%" height="225" src={urlToImage}></img>
                            <div className="card-body">
                                <p className="card-text">{title}</p>
                                <div className="d-flex justify-content-between align-items-center">
                                    <div className="btn-group">
                                        <Link as={`${url}`} href={`${url}`}>
                                            <button type="button" className="btn btn-sm btn-outline-secondary">View Detail</button>
                                        </Link>
                                    </div>
                                    <small className="text-muted">{source.name}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    </div>
}

Entertainment.getInitialProps = async function (e) {
    const res = await fetch('https://newsapi.org/v2/top-headlines?country=id&category=entertainment&apiKey=4e5238e3dd7b4b8ebea90b4fd5ec98c7');
    const data = await res.json();

    return {
        articles: data.articles,
        page: parseInt(data.totalResults),
    }
}

export default Entertainment;
